"""Unit tests for plot submodule"""

import pytest
import datetime
from floodsystem.plot import plot_on_map, plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels


def test_plot_water_levels():
    stations = build_station_list()
    update_water_levels(stations)

    #Test does not take more than 6 stations
    max_num = 6
    with pytest.raises(ValueError):
        plot_water_levels(station=stations[0:max_num + 1], dates=None, levels=None, given_as_list=True)


    # Can not figure out how to suppress the output from matplotlib necessary to test
    """
    station_list = []
    dates_list = []
    levels_list = []

    for i in range(0, max_num):
        if(stations[i].latest_level is not None):
            dates, levels = fetch_measure_levels(stations[i].measure_id, dt=datetime.timedelta(days=10))

            station_list.append(stations[i])
            dates_list.append(dates)
            levels_list.append(levels)

    # check can be called for multiple values
    plot_water_levels(station_list, dates_list, levels_list, given_as_list=True, test=True)

    # check can be called for a single value
    plot_water_levels(station_list[0], dates_list[0], levels_list[0], given_as_list=False, test=True)
    """


            

