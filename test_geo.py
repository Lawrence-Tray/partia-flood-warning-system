"""Unit tests for geo submodule"""

import pytest
from floodsystem.geo import (
    stations_by_distanceB,
    stations_within_radius, 
    rivers_with_station, 
    stations_by_river,
    rivers_by_station_number
)
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation


def test_stations_by_distanceB():
    stations = build_station_list()
    p = (51.874767, -1.740083)
    assert stations_by_distanceB(stations,p)[0][0][0] == "Bourton Dickler"


def test_stations_within_radius():
    stations = build_station_list()
    centre = (51.874767, -1.740083)
    r = 0.001
    swr = stations_within_radius(stations, centre, r)
    assert swr[0] == 'Bourton Dickler'
    assert len(swr) == 1

def test_rivers_with_station():
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    assert rivers[43] < rivers[44]


def test_stations_by_river():
    stations = build_station_list()
    Z = "Thames"
    d = stations_by_river(stations)
    assert Z in d


def test_rivers_by_station_number():
    
    #build list of stations
    stations = build_station_list()
    
    # check raises error for large N
    with pytest.raises(ValueError):
        rivers_by_station_num = rivers_by_station_number(stations, len(stations))

    # find first ten rivers with highest number of stations
    # returned as list of tuples: (river_name, number_of_stations)
    rivers_by_station_num = rivers_by_station_number(stations, 10)

    # test first river is the Thames
    assert rivers_by_station_num[0][0] == "Thames"

    # test Thames station number within expected range 40 - 70
    # range kept large to protect against future modification
    assert rivers_by_station_num[0][1] > 40 and rivers_by_station_num[0][1] < 70

    # test in descending order by station number
    for i in range(0, len(rivers_by_station_num) - 1):
        assert rivers_by_station_num[i][1] >= rivers_by_station_num[i+1][1]
