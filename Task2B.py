from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
import operator

tol = 0.8
def run():
    stations = build_station_list()
    update_water_levels(stations)

    k = stations_level_over_threshold(stations, tol)

    for member in k:
        print(str(member[0]) + ": " + str(round(member[1], 3)))

if __name__ == "__main__":
    print("***Task 2B: CUED Part 1A Flood Warning System***")
    run()