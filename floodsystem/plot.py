"""submodule for plotting functions"""

import plotly
from plotly.graph_objs import Scattermapbox, Layout, Data, Marker
plotly.tools.set_credentials_file(username="lawrence-tray", api_key="OU1yfuMYkqgYffTRrN4D")

from .station import MonitoringStation
from .utils import hsv2rgb

import math
import matplotlib
import matplotlib.pyplot as plt
import datetime

from .analysis import polyfit
from .datafetcher import fetch_measure_levels


def gen_colours(ratios):
    """Generate corresponding set of colours 
    for use in plotting
    
    """
    
    upper_cutoff = 0.8 # prevents looping back into red
    colours = []

    for ratio in ratios:
        r, g, b = hsv2rgb(ratio * upper_cutoff, 1, 1)
        colours.append("rgb({0},{1},{2})".format(r, g, b))
        
    return colours


def gen_ratios(vals):
    """Express vals as a ratio of 
    distance between min and max
    """

    dispersion_factor = 1.5
    max_val = max(vals)
    min_val = min(vals)
    ratios = []

    for v in vals:
        ratio = (v - min_val)/(max_val - min_val)
        ratio = ratio ** (1/dispersion_factor) # make distribution more even
        ratios.append(ratio)

    return ratios


def extract_av_rainfall(station):
    """return avergae rainfall for a station
    """
    return (station.typical_range[0] + station.typical_range[1]) / 2


def plot_on_map(stations, f = extract_av_rainfall, g = extract_av_rainfall, title="Flood Risk Level", ratio_made=False):
    """Plot all stations with available data on map as a series of circles, 
    colour is proportional to f(station)
    size is proprtional to g(station)

    """
    
    mapbox_Access_token = "pk.eyJ1IjoibGF3cmVuY2UtdHJheSIsImEiOiJjamNzNGV0eGEwOWd5MnF0NmdzN3YzY2MxIn0.SINMLoRnYwHcmQIOcKDvrA"

    latitudes = []
    longitudes = []
    col_vals = []
    size_vals = []
    sizes = []
    captions = []

    for s in stations:
        if (s.typical_range != None):     
            col_val = f(s)
            size_val = g(s)

            latitudes.append(s.coord[0])
            longitudes.append(s.coord[1])
            col_vals.append(col_val)
            size_vals.append(size_val)
            captions.append(s.name + ": " + str(round(col_val, 2)))

    # generate colour ratios
    if(not ratio_made):
        col_ratios = gen_ratios(col_vals)
        size_ratios = gen_ratios(size_vals)
    else:
        col_ratios = col_vals
        size_ratios = size_vals

    # generate corresponding colours 
    colours = gen_colours(col_ratios)    

    # generate sizes
    sizes = map( lambda x : 5 + 40 * x, size_ratios)
    sizes = list(sizes) # cast to list

    data = Data([
        Scattermapbox(
            lat=latitudes,
            lon=longitudes,
            mode='markers',
            marker=Marker(
                size=sizes,
                color=colours,
                opacity = 0.7
            ),
            text=captions
        )
    ])

    layout = Layout(
        title=title,
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            accesstoken=mapbox_Access_token,
            bearing=0,
            center=dict(
                lat=53,
                lon=-2
            ),
            pitch=0,
            zoom=5,
            style='light'
        ),
    )

    fig =dict(data=data, layout=layout)
    plotly.offline.plot(fig, filename='Map_Plot.html')


def plot_water_levels_for_station(station, days=5, flooding=True):
    """for the supplied station, plot the water 
    levels for a certain number of days back
    flooding boolean hides historical minimum
    """

    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=days))
    p, d0 = polyfit(dates, levels, 3)
    plot_water_level_with_fit(station, dates, levels, p, flooding)


def plot_water_levels(station, dates, levels, given_as_list=False, test=False):
    """Plot water levels for the given station (or list of stations) over the dates given"""

    if (test == True):
        matplotlib.use('Agg')        

    if given_as_list:
        n = len(station) # number of items to display

        if(n > 6):
            raise ValueError("Supplied more than 6 items to plot")

        cols = 2 # 2 columns
        rows = (n // cols) + (n % 2) # number of rows

        f , axarr = plt.subplots(rows, cols, sharex=True)
        f.suptitle("Water level history for following stations")

        for i in range(0, n):
            r = i // cols
            c = i % cols

            axarr[r, c].plot(dates[i], levels[i])
            axarr[r, c].set_title(station[i].name)
            axarr[r, c].set(
                title=station[i].name, 
                xlabel='Date', 
                ylabel='Water Level / m'
                )
        
        # Set rotation of date ticks
        for ax in axarr.flat:
            plt.setp(ax.get_xticklabels(), rotation=45)

    else:
        # Plot levels against dates
        plt.plot(dates, levels)

        # Add labels
        plt.xlabel('Date')
        plt.ylabel('Water level / m')
        plt.xticks(rotation=45)
        plt.title(station.name)

    # Display
    plt.tight_layout()
    plt.show()


def plot_water_level_with_fit(station, dates, levels, p, flooding = True):
    x = dates

    if isinstance(x[0], datetime.datetime):
        x = matplotlib.dates.date2num(x)
    
    x0 = x[0]
    y = x - x0

    plt.xlabel('$dates$')
    plt.ylabel('$levels$')
    plt.title(station.name)
    plt.xticks(rotation=45)

    plt.plot(dates, levels)
    plt.plot(dates, p(y), '--')

    if(not flooding):
        plt.plot(dates, y * 0 + station.max_range[0], '--')
        
    
    plt.plot(dates, y * 0 + station.typical_range[0], '--')
    plt.plot(dates, y * 0 + station.typical_range[1], '--')
    plt.plot(dates, y * 0 + station.max_range[1], '--')

    plt.tight_layout()
    if(not flooding):
        plt.legend(["Real values", "Fitted curve", "Historical Min", "Typical Min", "Typical Max", "Historical Max"])
    else:
        plt.legend(["RealValues", "Fitted curve", "Typical Min", "Typical Max", "Historical Max"])
    plt.show()
