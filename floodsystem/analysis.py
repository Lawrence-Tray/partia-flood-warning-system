"""Analysis submodule"""
import matplotlib
import numpy as np
import datetime


def polyfit(dates, levels, p):
    """ Returns best fit polynomial of degree p
    to predict level from date
    
    """

    #time shift
    d0 = dates[0]

    #check if given as datetime or already converted
    if isinstance(d0, datetime.datetime):
        x = matplotlib.dates.date2num(dates)
        x0 = x[0]
    else:
        x = dates
        x0 = dates[0]

    p_coeff = np.polyfit(x-x0, levels, p)

    poly = np.poly1d(p_coeff)

    return poly, d0

