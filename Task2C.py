from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level
import operator
import pprint

def run():
    stations = build_station_list()
    update_water_levels(stations)
    N = 10
    x = stations_highest_rel_level(stations, N)
    z = []
    for station in x:
        z.append((station.name, station.relative_water_level()))
        
    for member in z:
        print(str(member[0]) + ": " + str(member[1]))


if __name__ == "__main__":
    print("***Task 2C: CUED Part 1A Flood Warning System***")
    run()