import pytest
from floodsystem.analysis import (
    polyfit
)
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
import datetime
import numpy as np


def test_polyfit():
    #now = datetime.datetime.utcnow()
   # one_day = datetime.timedelta(days=1)

   # z = []

    #for i in range(1,11):
       # w = now + one_day*(i-1)
       # z.append(w)

    x = [0,1,2,3,4,5,6,7,8,9,10]
    x1 = np.array(x)
    y = [0,1,4,9,16,25,36,49,64,81,100]
    y0 = np.array(y)

    poly, d0 = polyfit(x1,y0,3)
    assert round(poly(23), 0) == 23**2