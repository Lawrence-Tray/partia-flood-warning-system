import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels


def run():
    """Requirements for Task 2E"""

    # Constants
    number_of_stations = 6 # number of stations to examine
    dt = 10 # number of days into past

    # Build list of stations
    print("Building list of stations...")
    stations = build_station_list()

    # Update latest level
    print("Updating latest levels...")
    update_water_levels(stations)

    valid_stations = []

    # Remove None instances of latest level
    for i in range(0, len(stations)):
        if stations[i].latest_level is not None:
            valid_stations.append(stations[i])

    # Sort by descending latest level
    print("Sorting by water level...")
    stations = valid_stations
    stations.sort(key = lambda x: x.latest_level, reverse=True)

    # Slice to extract highest level stations
    stations = stations[0:number_of_stations]  
    
    # Define arrays
    station_list = []
    dates_list = []
    levels_list = []

    # Fetch data for past dt days
    print("Fetching more level data...")
    for s in stations:
        dates, levels = fetch_measure_levels(s.measure_id, dt=datetime.timedelta(days=dt))
        
        station_list.append(s)
        dates_list.append(dates)
        levels_list.append(levels)

    print("Plotting results...")
    plot_water_levels(station_list, dates_list, levels_list, given_as_list=True)


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()