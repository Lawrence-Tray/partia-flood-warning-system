"""Code for Task1F which prints all the 
names of stations with inconsistent data"""

from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1F"""

    #build stations list
    stations = build_station_list()

    #get list of inconsistent stations
    inconsistent = inconsistent_typical_range_stations(stations)
    names = []
    
    #extract names
    for i in inconsistent:
        names.append(i.name)
    
    #sort alphabetically
    names.sort()

    print(names)


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()