from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list


#point of centre
centre = (52.2053, 0.1218)

#radius distance
r = 10

#creates the intial data
stations = build_station_list()

#print list returned by this function
stats = stations_within_radius(stations, centre, r)
stats = sorted(stats)
print(stats)