from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels_for_station


def run():
    # Visual Studio Code does not capture user input >> must enter names manually
    
    # get input
    # name = input("Name of station to inspect: ")
    # dt = int(input("Number of days back: "))

    name = 'Huntingford Bridge'
    dt = 5

    # build list of stations
    print("Building station list...")
    stations = build_station_list()

    # update latest water levels
    print("Updating latest water level...")
    update_water_levels(stations)

    for station in stations:
        if(station.name == name):
            print("Risk level: " +  str(station.risk_level))
            plot_water_levels_for_station(station, dt, flooding = False)
            return

    print("No such station found")


if __name__ == "__main__":
    run()