"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
import datetime
import matplotlib
from .analysis import polyfit
from .datafetcher import fetch_measure_levels 


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town, catchment_name, date_opened, max_range):

        self._station_id = station_id
        self._measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self._name = label
        if isinstance(label, list):
            self._name = label[0]

        self._coord = coord
        self._typical_range = typical_range
        self._river = river
        self._town = town
        
        self.latest_level = None

        #additional custom props
        self._catchment_name = catchment_name
        self._date_opened = date_opened
        self._max_range = max_range
        self._risk_level = None
        

    def __repr__(self):
        d =  "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}\n".format(self.typical_range)
        #additional custom props
        d += "   max range:     {}\n".format(self.max_range)
        d += "   catchment area:{}\n".format(self.catchment_name)
        d += "   date opened:   {}".format(self.date_opened)
        return d

    
    def typical_range_consistent(self):
        """checks station object's typical range exists 
        and the min is less than or equal to the max
        returns true if typical range consistent"""

        if self.typical_range is None: # object does not exists
            return False
        elif self.typical_range[0] > self.typical_range[1]: # min is greater than max
            return False
        else: # all tests passed
            return True


    def relative_water_level(self):
        if self.typical_range == None: # object does not exists
            return None
        if self.latest_level == 0:
            return None
        if self.latest_level == None:
            return None

        elif self.latest_level < self.typical_range[0]:
            return None
        else: 
            ratio = ((self.latest_level - self.typical_range[0])/(self.typical_range[1]- self.typical_range[0]))
            return ratio


    #For calculating flood risk
    def calculate_risk_level(self):
        """Calculates float from 0-1 
        assessing the flood risk level:

            0.0 - 0.2: high drought risk
            0.2 - 0.4: low drought risk
            0.4 - 0.6: normal conditions
            0.6 - 0.8: low flood risk
            0.8 - 1.0: high flood risk

        Makes calculation based off following attributes:

            typical_range,
            max_range,
            polyfit() of degree 3
            Based off past 5 days
            Projects forward 5 days

        """

        # arbitrary constants
        DAYS_BACK = 5
        DAYS_FORWARD = 5
        DEGREE = 3
        MIN_THRESHOLD = 0
        MAX_THRESHOLD = 0.8

        # Time saving necessary
        # if latest level well within typical
        # then do not perform costly analysis
        rwl = self.relative_water_level()
        if (rwl is None):
            return 0.5 # cannot perform analysis
        elif (rwl < MAX_THRESHOLD and rwl > MIN_THRESHOLD):
            return 0.4 + 0.2 * rwl

        # check variable exists
        if (self.max_range is None or self.typical_range is None):
            return 0.5 # cannot calculate

        # check held data is valid
        highest = self.max_range[1]
        high = self.typical_range[1]
        low = self.typical_range[0]
        lowest = self.max_range[0]

        if (highest > high and high > low and low > lowest):
            pass
        else:
            return 0.5 # station data is invalid >> cannot assess risk level >> give medium



        dt = datetime.timedelta(days=DAYS_BACK)

        # get level history
        dates, levels = fetch_measure_levels(self.measure_id, dt=dt)
        if(len(dates) == 0):
            return 0.5 # API does not respond >> cannot assess risk

        # Generate polynomial
        try:
            poly, d0 = polyfit(dates, levels, DEGREE)
        except ValueError:
            return 0.5 # occasionally raises ValueError

        f0 = matplotlib.dates.date2num(d0)

        # get current date
        today = datetime.datetime.utcnow()
        
        # set time delta of one day
        one_day = datetime.timedelta(days=1)
        
        # generate future dates
        future_dates = []
        future_levels = []

        # get tomorrow
        tomorrow = today + one_day

        for i in range(0, DAYS_FORWARD):
            # generate future dates
            future_dates.append(tomorrow + (i * one_day))
            future_dates[i] = matplotlib.dates.date2num(future_dates[i])

            # generate predicted future levels
            future_levels.append(poly(future_dates[i] - f0))


        temp_risk = 0
        max_risk = 0
        min_risk = 1

        for i in range(0, DAYS_FORWARD):
            
            day_weighting = (DAYS_FORWARD - i) / DAYS_FORWARD
            level = future_levels[i]           

            # If predicted to exceed historical maximum 
            if level > highest:
                # At least severe risk and more so if flooding soon
                temp_risk = 0.8 + 0.2 * day_weighting # at least severe and more so if flooding soon

            # If predicted to exceed typical maximum
            elif level > high:
                # At least high risk and more so if closer to highest
                temp_risk = 0.6 + 0.2 * (level - high) / (highest - high)
                
            # If predicted to be within typical range
            elif level > low:
                # At least low and more so if closer to high end
                temp_risk = 0.4 + 0.2 * (level - low) / (high - low) 

            # If below typical range
            elif level > lowest:
                # At 
                temp_risk = 0.2 + 0.2 * (level - lowest) / (low - lowest)

            # If below historic minimum
            else:
                temp_risk = 0.2 - 0.2 * day_weighting

            # Feed into max and min values
            if temp_risk > max_risk:
                max_risk = temp_risk

            if temp_risk < min_risk:
                min_risk = temp_risk

        # If likely to flood
        if max_risk > 0.5:
            return max_risk
        # Else will drought
        else:
            return min_risk


    @property
    def risk_level(self):
        """Get flood risk level 0-1"""
        if(self._risk_level is None):
            self._risk_level = self.calculate_risk_level()

        return self._risk_level


    #properties list
    #declared as decorators to make all attributes read-only
    #backing variables are marked private by leading underscore
    @property
    def station_id(self):
        """Get station id"""
        return self._station_id

    @property
    def measure_id(self):
        """Get measure id"""
        return self._measure_id

    @property
    def name(self):
        """Get station name"""
        return self._name

    @property
    def coord(self):
        """Get coordinate"""
        return self._coord

    @property
    def typical_range(self):
        """Get typical range as tuple (min, max)"""
        return self._typical_range

    @property
    def river(self):
        """Get river"""
        return self._river

    @property
    def town(self):
        """Get town"""
        return self._town

    #additional custom props

    @property
    def catchment_name(self):
        """Get catchment name"""
        return self._catchment_name

    @property
    def date_opened(self):
        """Get date opened"""
        return self._date_opened

    @property
    def max_range(self):
        """Get max range as tuple (min, max)"""
        return self._max_range


def inconsistent_typical_range_stations(stations):
    """Returns list of stations with inconsistent range data"""

    inconsistent = []
    
    for s in stations:
        if (s.typical_range_consistent()):
            pass
        else:
            inconsistent.append(s)

    return inconsistent