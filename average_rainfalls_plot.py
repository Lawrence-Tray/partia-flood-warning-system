"""file to display station locations on a map"""


from floodsystem.stationdata import build_station_list
from floodsystem.plot import plot_on_map


def run():
    stations = build_station_list()
    plot_on_map(stations, title="Average Rainfall")


if __name__ == "__main__":
    print("*** Extension: plot all stations on a map ***")
    run()