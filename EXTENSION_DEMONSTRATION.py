from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_on_map


def run():
    """Extension code"""

    # build list of stations
    print("Building station list...")
    stations = build_station_list()

    # update latest water levels
    print("Updating latest water level...")
    update_water_levels(stations)

    # calculating flood risk
    print("Calculating flood/drought risk for each station...")
    for idx, station in enumerate(stations):
        # calling attribute for first time sets property
        # do it manually to see progress
        temp = station.risk_level 
        
        # display ongoing progress
        # runs slower when many stations have high water levels
        if(idx % 500 == 0):
            print("    " + str(idx) + " / " + str(len(stations)))

    # display flood risk of stations on a map
    plot_on_map(
        stations, 
        f= lambda x: x.risk_level, 
        g = lambda x : 2 * abs(0.5 - x.risk_level), 
        title = "FLOOD AND DROUGHT WARNINGS (scale of 0-1 : red-purple : drought-flood)",
        ratio_made=True
        ) 


if __name__ == "__main__":
    print("*** EXTENSION TASK ***""")
    run()