# CUED Part IA Flood Warning System

This is the Part IA Lent Term computing activity at the Department of
Engineering, University of Cambridge.

The activity is documented at
https://cued-partia-flood-warning.readthedocs.io/. Fork this
repository
(https://bitbucket.org/CUED/partia-flood-warning-system/fork) to start
the activity.

# Extension

## Setup

Before running the extension, ensure the following packages are installed on the local version of python:

* plotly
* matplotlib
* haversine
* requests
* dateutils

## Running

Clone the repository onto your local machine.

Simply run the EXTENSION_DEMONSTRATION.py file located within the root directory.

Once the program runs it will output an html file, which the system may attempt to open automatically - in which case pick your browser of choice and click ok. 

If it does not auto-open, search the root directory for Map_Plot.html and open this manually.

## Description

The basic MonitroingStation object was edited to also store historic maximum and minimum water levels. This allowed the flood risk assessment algorithm to be extended to deal with droughts also.

For details of this please refer to the calculate_risk_level method of the MonitoringStation class.

The result is a float 0-1, where 0 represents severe drought risk, 0.5 is a normal level and 1 is severe flood risk.

All this data is then plotted on a map using plotly where the colour of each circle denotes whether the station is in drought or flood and its size represents the severity of either event.
