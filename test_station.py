"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import update_water_levels


def test_create_monitoring_station():

    #Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    catchment_name = "some catchment area"
    date_opened = "1/1/2018"
    max_range = (-4, 8)

    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town, catchment_name, date_opened, max_range)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    assert s.catchment_name == catchment_name
    assert s.date_opened == date_opened
    assert s.max_range == max_range


def test_readonly_attributes():

    #Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    catchment_name = "some catchment area"
    date_opened = "1/1/2018"
    max_range = (-4, 8)

    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town, catchment_name, date_opened, max_range)

    with pytest.raises(Exception):
        s.station_id = 0

    with pytest.raises(Exception):
        s.measure_id = 0

    with pytest.raises(Exception):
        s.name = 0

    with pytest.raises(Exception):
        s.coord = 0

    with pytest.raises(Exception):
        s.typical_range = 0

    with pytest.raises(Exception):
        s.river = 0

    with pytest.raises(Exception):
        s.town = 0

    with pytest.raises(Exception):
        s.catchment_name = 0

    with pytest.raises(Exception):
        s.date_opened = 0

    with pytest.raises(Exception):
        s.max_range = 0


def test_inconsistent_typical_range_stations():

    #Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"
    catchment_name = "some catchment area"
    date_opened = "1/1/2018"
    max_range = (-4, 8)

    trange = (4, 3) # inconsistent as min > max
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town, catchment_name, date_opened, max_range)
    
    trange = None # inconsistent as does not exist
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town, catchment_name, date_opened, max_range)

    inconsistent_stations = inconsistent_typical_range_stations([s1, s2])

    #check both items are returned
    assert len(inconsistent_stations) == 2


def test_risk_level():
    """basic tests for risk level algorithm"""

    #Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"
    catchment_name = "some catchment area"
    date_opened = "1/1/2018"
    
    # inconsistent as typical min > max
    max_range = (0, 8)
    typical_range = (4, 3) 
    station = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town, catchment_name, date_opened, max_range)
    
    assert station.risk_level == 0.5 # not sufficient data

    # inconsistent as max min > max
    max_range = (8, 0) 
    typical_range = (3, 4)
    station = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town, catchment_name, date_opened, max_range)

    assert station.risk_level == 0.5 # not sufficient data

    # expect 0.55 risk when relative_water_level = 0.75
    max_range = (0,8)
    typical_range = (2,4)
    station = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town, catchment_name, date_opened, max_range)

    station.latest_level = 3.5 # relative water level would be 0.75 >> gives flood risk of 0.55

    assert station.risk_level == 0.55

    #cannot spoof level data >> cannot test polyfit
    max_range = (0,8)
    typical_range = (2,4)
    station = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town, catchment_name, date_opened, max_range)

    station.latest_level = 5 # above typical range >> will make call to API and fail as bogus measure_id

    with pytest.raises(Exception):
        temp = station.risk_level

