import pytest
from floodsystem.flood import (
    stations_level_over_threshold,
    stations_highest_rel_level
)
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation


def test_stations_level_over_threshold():
    stations = build_station_list()
    tol = 0.9
    x = stations_level_over_threshold(stations,tol)
    for station in x:

        assert station.relative_water_level() > tol


def test_stations_highest_rel_level():
    stations = build_station_list()
    N = 15
    y = stations_highest_rel_level(stations, N)
    z = []
    for station in y:
        z.append(station.relative_water_level())
        
    i = len(y) - 1
    for x in range (0, i):
        assert z[x]>z[x+1]
