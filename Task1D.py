from floodsystem.geo import rivers_with_station
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river

def run():
    #creates the intial data
    stations = build_station_list()

    #print toal number of rivers
    print("Total number of rivers: " + str(len(rivers_with_station(stations))))

    print("First 10 rivers alphabetically  with stations on them\n")
    print(rivers_with_station(stations)[0:10])
    print()

    #dictionary of rivers and stations
    river_dict = stations_by_river(stations) 

    print("Individual stations on following rivers \n")

    A = 'River Cam'
    print(A)
    print(river_dict[A])
    print()

    B = 'River Aire'
    print(B)
    print(river_dict[B])
    print()

    C = 'Thames'
    print(C)
    print(river_dict[C])
    print()

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()