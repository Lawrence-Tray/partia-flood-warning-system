from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_on_map, plot_water_levels_for_station
from floodsystem.utils import sorted_by_key


def calc_town_flood_risk(stations):
    """Returns dictionary of items with
    key: town name
    value: flood level risk for that town 
    
    """
    
    town_risks = {}

    #use dictionary as constant time lookup
    low_risks = {}

    print("Calculating risks for each station...")

    size = len(stations)

    # populate dictionary
    for idx, s in enumerate(stations):
        # For progress
        if(idx % 500 == 0):
            print(str(idx) + " / " + str(size))
            
        if s.town is not None:
            # if town not marked as high risk
            if s.town not in low_risks:
                # if key not present >> add
                if s.town not in town_risks:
                    #print("  New town: " + str(s.town))
                    town_risks[s.town] = []

                # append risk value
                town_risks[s.town].append(s.risk_level)

                # speed up by doing a lazy search
                # if any station in the town shows low risk
                # then stop calculating
                if(s.risk_level < 0.4):
                    # add key to dictionary
                    #print("  Discard town: " + str(s.town))
                    low_risks[s.town] = None
                
    print("Taking average for each town...")
    # take average of flood risk level for surrounding stations
    for key in town_risks:
        # list of all risk values
        vals = town_risks[key]

        # take simple mean of these
        town_risks[key] = sum(vals) / float(len(vals))

    return town_risks          


def run():
    """Requirements for Task 2G"""
    
    # build list of stations
    print("Building station list...")
    stations = build_station_list()

    # update latest water levels
    print("Updating latest water level...")
    update_water_levels(stations)

    # calculate town flood risks
    print("Calculating town flood risk:") 
    town_risk = calc_town_flood_risk(stations)

    #array of high risk towns
    high_risk_towns = []

    # print town names with high-sever flood risk
    for key in town_risk:
        risk_level = town_risk[key]

        if (risk_level > 0.6): # flood risk level is high or severe
            high_risk_towns.append([key, risk_level])

    high_risk_towns = sorted_by_key(high_risk_towns, 1, reverse=True)

    print("\nTOWNS AT RISK OF FLOODING:")

    for item in high_risk_towns:
        print("    "+ item[0] + ": " + str(round(item[1], 3)))

        for station in stations:
            if(station.town == item[0]):
                if(station.risk_level >= item[1]):
                    plot_water_levels_for_station(station)
                    break    
                    

    # display flood risk of stations on a map
    plot_on_map(
        stations, 
        f= lambda x: x.risk_level, 
        g = lambda x : 2 * abs(0.5 - x.risk_level), 
        title = "Flood / Drought warnings",
        ratio_made=True
        )       
            

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()