#Task1B.py

from floodsystem.geo import stations_by_distanceB
from haversine import haversine

from floodsystem.stationdata import build_station_list


#point from which distances are calculated
p = (52.2053, 0.1218)

#creates initial data
stations = build_station_list()

print(stations)

#print the list returned by this function
print(stations_by_distanceB(stations, p))