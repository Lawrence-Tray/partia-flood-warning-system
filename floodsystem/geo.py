"""This module contains a collection of functions related to
geographical data.

"""


from .utils import sorted_by_key
from haversine import haversine
from floodsystem.stationdata import build_station_list
import operator
import math


def stations_by_distanceB(stations,p):
    #create empty list  
    a = []
    #iterate over all of the stations
    for station in stations:
        #add a tuple to the end of the list (name, town, coordinate)
        a.append((station.name, station.town, (station.coord)))  
    #create empty list
    b =[]
    #iterate over the members in a
    for member in a:
        #add a tuple to the end of the list (name, town, distance from point p)
        b.append((member[0], member[1], haversine(member[2], p)))

    #sort list by distance from point
    b.sort(key=operator.itemgetter(2))

    #for task B, c demonstrates top 10 and bottom ten of list
    c = [b[0:10], b[-10:]]

    #for task B
    return c    

def stations_by_distanceC(stations,p):
    #create empty list  
    a = []
    #iterate over all of the stations
    for station in stations:
        #add a tuple to the end of the list (name, town, coordinate)
        a.append((station.name, station.town, (station.coord)))  
    #create empty list
    b =[]
    #iterate over the members in a
    for member in a:
        #add a tuple to the end of the list (name, town, distance from point p)
        b.append((member[0], member[1], haversine(member[2], p)))

    #sort list by distance from point
    b.sort(key=operator.itemgetter(2))


    #for task C
    return b


def stations_within_radius(stations, centre, r):
    #call function stations_by_distance into a list
    f =  stations_by_distanceC(stations, centre)
  
    #creaty empty list
    h = []
    #iterate over members in list f
    for member in f:
        #if distance is over selected radius, r
        if member[2] < r:
            #add the name of that station into the list
            h.append(member[0])
        #if not
        else:
            #break the loop
           break


    return h


#create a function to creat a list of rivers
def rivers_with_station(stations):
    #create an empty list
    s = []
    #iterate over all the stations
    for station in stations:
        #add all of the river names including duplicates
        s.append(station.river)
    #make it into a set to remove duplicates
    t = set(s)
    #turn set back into a list
    u = list(t)
    #sort list into alphabetical order
    u.sort()
    #return all items in list
    return u


#function to return dictionary with river name as key and stations on that river as value
def stations_on_river_Z(stations, Z):
    #create empty list
    k = []
    #iterate over stations
    for station in stations:
        #create list of tuples (name, river)
        k.append((station.name, station.river))
    #create empty list
    i = []
    #iterate over members
    for member in k:
        #if the river name for a certain station is Z
        if member[1] == Z:
            #add station name to the list i
            i.append(member[0])
        #if not
        else:
            #ignore
             pass
    #sort the list of station names on that river in alphabetical order
    i.sort(key=operator.itemgetter(0))
    #create a dictionary with key as river name and value as list i
    j = {Z : i}

    return j


def stations_by_river(stations):
    """returns dictionary with river name as the key
    and a list of station names on the river as the value

    """
    # NB: this algorithm is painfully slow 
    # but needed to integrate with stations_on_river_Z
    # which was laready written

    rivers = rivers_with_station(stations)
    river_dictionary = {}

    for river_name in rivers:
        river_dictionary.update(stations_on_river_Z(stations, river_name))

    return river_dictionary


def rivers_by_station_number(stations, N):
    """Returns the N rivers with the greatest number of monitoring stations
    If tied for Nth place, displays all tied rivers
    Returns list of tuples of form: (river name, number of stations)

    """

    #extract dictionary of form [key:river_name, value:List(stations)]
    river_dictionary = stations_by_river(stations)
    #list of tuples of form (river_name, number_of_stations)
    river_sizes = []

    for key, value in river_dictionary.items():
        river_sizes.append((key, len(value)))

    #sorts river_size in descending order of station number
    river_sizes = sorted_by_key(x=river_sizes, i=1, reverse=True)

    #raises error if N exceeds the number of rivers
    number_of_rivers = len(river_sizes)
    if(N > number_of_rivers):
        raise ValueError("Value of N supplied, exceeds the total number of rivers with stations")

    #set up variables for while loop
    ongoing = True
    i = N - 1
    min_val = river_sizes[i][1]

    #increments i until values not equal
    while(ongoing):
        i += 1
        if (i == number_of_rivers):
            ongoing = False
        elif (river_sizes[i][1] != min_val):
            ongoing = False

    #returns first i rivers with highest number of stations
    return river_sizes[0:i]
