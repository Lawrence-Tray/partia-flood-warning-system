from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1E"""

    #build list of stations
    stations = build_station_list()
    
    #build tuple list of first 9 rivers with highest number of stations
    river_list_by_station_number = rivers_by_station_number(stations, 10)

    #output result
    print(river_list_by_station_number)


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()