from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import polyfit
from floodsystem.flood import stations_highest_rel_level
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import matplotlib
import numpy as np


def run():

    p = 3
    N = 5
    dt = 2

    stations = build_station_list()
    update_water_levels(stations)
    x = stations_highest_rel_level(stations, N)

    for station in x:
            dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
            w = matplotlib.dates.date2num(dates)

            if(len(w) > 0):
                poly, d0 = polyfit(w,levels, p)
 
                plot_water_level_with_fit(station, w, levels, poly)
                 
if __name__ == "__main__":
    print("***Task 2F: CUED Part 1A Flood Warning System***")
    run()

             
