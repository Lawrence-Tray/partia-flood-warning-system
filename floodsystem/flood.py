import operator

def stations_level_over_threshold(stations, tol):
    r = []
    for station in stations:
        if station.relative_water_level() == None:
            pass
        elif station.relative_water_level() > tol:
            r.append((station.name, station.relative_water_level()))
            r.sort(key=operator.itemgetter(1), reverse=True)
        
    return r


def stations_highest_rel_level(stations, N):
    q = []
    for station in stations:
        if station.relative_water_level() == None:
            pass
        else:
            q.append(station)
    q.sort(key = lambda x : x.relative_water_level(), reverse=True)
    p = q[:N]
    return p



